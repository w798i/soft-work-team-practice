﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sliderScript : MonoBehaviour
{
    public Slider slider;
    public Text text;
    public float voice;

    // Start is called before the first frame update
    void Start()
    {
        voice = GameManager.voice;
        slider.value = GameManager.voice;
        text.text = GameManager.voice.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        voice = slider.value;
        GameManager.voice = slider.value;
        text.text = voice.ToString();
    }
}
