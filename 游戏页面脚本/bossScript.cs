﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossScript : MonoBehaviour
{
    public int heart = 35;
    private int hurt = 1;
    public GameObject die;
    public GameObject gem;
    public GameObject player;
    public GameObject bullet;
    private float bulletSpeed = 10f;
    private float attackTime = 1.5f;
    private float superattackTime = 1.5f;
    public int score = 50;
    public bool isVisible = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        attackTime = attackTime + Time.fixedDeltaTime;
        superattackTime = superattackTime + Time.fixedDeltaTime;
        if (isVisible)
        {
            if (superattackTime >= 5f&&heart<=20)
            {
                SuoerAttack();
                superattackTime = 0;
            }
            if (attackTime >= 1.2f && heart <= 20)
            {
                Attack();
                attackTime = 0;
            }
            else if (attackTime >= 0.8f && heart > 20)
            {
                Attack();
                attackTime = 0;
            }
        }
        else
        {
                superattackTime = 0;
                attackTime = 0;
        }
        if (heart == 0)
        {
            Die();
        }
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }

    private void OnBecameInvisible()
    {
        isVisible = false;
    }

    private void Die()
    {
        Instantiate(die, transform.position, transform.rotation);
        Instantiate(gem,transform.position, transform.rotation);
        Destroy(gameObject);
        GameManager.score = GameManager.score + score;
    }

    void Attack()
    {  
        if (heart <= 20)  
        {
            int a = Random.Range(0, 3);
            bulletSpeed = 15f;
            if (a == 0)
            {
                Vector2 position_a = new Vector2(transform.position.x - 5.5f, transform.position.y - 4.1f);
                GameObject bullet_a = Instantiate(bullet, position_a, Quaternion.identity) as GameObject;
                Vector2 position_b = new Vector2(transform.position.x - 5.5f, transform.position.y + 1.2f);
                GameObject bullet_b = Instantiate(bullet, position_b, Quaternion.identity) as GameObject;
                bullet_a.GetComponent<Rigidbody2D>().velocity = -1 * transform.right * bulletSpeed;
                bullet_a.transform.eulerAngles = new Vector3(0, 0, 90);
                bullet_b.GetComponent<Rigidbody2D>().velocity = -1 * transform.right * bulletSpeed;
                bullet_b.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            else if (a == 1)
            {
                Vector2 position_b = new Vector2(transform.position.x - 5.5f, transform.position.y + 1.2f);
                GameObject bullet_b = Instantiate(bullet, position_b, Quaternion.identity) as GameObject;
                Vector2 position_c = new Vector2(transform.position.x - 5.5f, transform.position.y + 5.5f);
                GameObject bullet_c = Instantiate(bullet, position_c, Quaternion.identity) as GameObject;
                bullet_c.GetComponent<Rigidbody2D>().velocity = -1 * transform.right * bulletSpeed;
                bullet_c.transform.eulerAngles = new Vector3(0, 0, 90);
                bullet_b.GetComponent<Rigidbody2D>().velocity = -1 * transform.right * bulletSpeed;
                bullet_b.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            else
            {
                Vector2 position_a = new Vector2(transform.position.x - 5.5f, transform.position.y - 4.1f);
                GameObject bullet_a = Instantiate(bullet, position_a, Quaternion.identity) as GameObject;
                Vector2 position_c = new Vector2(transform.position.x - 5.5f, transform.position.y + 5.5f);
                GameObject bullet_c = Instantiate(bullet, position_c, Quaternion.identity) as GameObject;
                bullet_a.GetComponent<Rigidbody2D>().velocity = -1 * transform.right * bulletSpeed;
                bullet_a.transform.eulerAngles = new Vector3(0, 0, 90);
                bullet_c.GetComponent<Rigidbody2D>().velocity = -1 * transform.right * bulletSpeed;
                bullet_c.transform.eulerAngles = new Vector3(0, 0, 90);
            }
        }
        else
        {
            int a = Random.Range(0, 6);
            bulletSpeed = 15f;
            if (a == 0)
            {
                Vector2 position_a = new Vector2(transform.position.x - 5.5f, transform.position.y +5.5f);
                GameObject bullet_a = Instantiate(bullet, position_a, Quaternion.identity) as GameObject;
                bullet_a.GetComponent<Rigidbody2D>().velocity = -1 * transform.right * bulletSpeed;
                bullet_a.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            else if (a == 1||a==2)
            {
                Vector2 position_b = new Vector2(transform.position.x - 5.5f, transform.position.y + 1.5f);
                GameObject bullet_b = Instantiate(bullet, position_b, Quaternion.identity) as GameObject;
                bullet_b.GetComponent<Rigidbody2D>().velocity = -1 * transform.right * bulletSpeed;
                bullet_b.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            else
            {
                Vector2 position_c = new Vector2(transform.position.x - 5.5f, transform.position.y  -4.1f);
                GameObject bullet_c = Instantiate(bullet, position_c, Quaternion.identity) as GameObject;
                bullet_c.GetComponent<Rigidbody2D>().velocity = -1 * transform.right * bulletSpeed;
                bullet_c.transform.eulerAngles = new Vector3(0, 0, 90);
            }
        }
    }

    void SuoerAttack()
    {
        Vector2 position = new Vector2(transform.position.x - 5.5f, transform.position.y - 1.5f);
        Vector3 playerPosition = player.transform.position;
        float m_fireAngle = Vector2.Angle(playerPosition - this.transform.position, Vector2.up);
        GameObject m_bullet = Instantiate(bullet, position, Quaternion.identity) as GameObject;
        m_bullet.GetComponent<Rigidbody2D>().velocity = ((playerPosition - transform.position).normalized *10f);
        m_bullet.transform.eulerAngles = new Vector3(0, 0, m_fireAngle);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Player": if (!playerScript.isPower && playerScript.count == 0) { playerScript.Hurt(hurt); } break;
            case "PlayerBullet": heart = heart - 1; break;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Player": if (!playerScript.isPower && playerScript.count == 0) { playerScript.Hurt(hurt); } break;
        }
    }
}
