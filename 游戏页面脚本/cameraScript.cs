﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraScript : MonoBehaviour
{
    public GameObject player;
    public GameObject air;
    private Cinemachine.CinemachineVirtualCamera cc;
    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<Cinemachine.CinemachineVirtualCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.transform.position.x >= 750)
        {
            this.transform.position = new Vector3(772,3.5f,-20);
            air.SetActive(true);
            cc.Follow = null;
            cc.m_Lens.OrthographicSize = 12;
        }
        else if(player.transform.position.x < 750)
        {
            air.SetActive(false);
            cc.Follow = player.transform;
            cc.m_Lens.OrthographicSize = 10;
        }
    }
}
