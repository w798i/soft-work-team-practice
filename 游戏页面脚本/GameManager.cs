﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public static bool isStop=false;
    public static int score;
    public Text scoreText;
    public GameObject heart1;
    public GameObject heart2;
    public GameObject heart3;
    public Text text;
    public Slider slider;
    public GameObject[] sign;
    public GameObject openPage;

    public static float voice=100;
    public static int count = 0;

    public static bool isGameover = false;
    public bool isSign = false;
    public bool isOk = false;

    public GameObject pausePage;
    public GameObject overPage;

    public Text best_Text;
    public Text score_Text;

    public Sprite sp1;
    public Sprite sp2;
    Image image1;
    Image image2;
    Image image3;

    // Start is called before the first frame update
    void Start()
    {
        image1 = heart1.GetComponent<Image>();
        image2 = heart2.GetComponent<Image>();
        image3 = heart3.GetComponent<Image>();
        score = 0;
        count = 0;
        isGameover = false;
        isStop = false;
        text.text = voice.ToString();
        slider.value = voice;
        sign[0].SetActive(true);
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (sign[0].activeInHierarchy)
        {
            Time.timeScale = 0;
            if (Input.GetKeyDown(KeyCode.E))
            {
                sign[0].SetActive(false);
                Time.timeScale = 1;
            }
        }
        Pause();
        GameOver();
        MusicVoice();
    }

    void FixedUpdate()
    {
        SetScore();
        SetHeart();
    }

    private void Pause()
    {
        int signCount = 0;
        for(int i = 0; i < sign.Length; i++)
        {
            if (sign[i].activeInHierarchy)
            {
                isSign=true;
            }
            else
            {
                signCount++;
            }
        }
        if (signCount == 7)
        {
            isSign = false;
        }
        if (!isSign)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && isStop || continueScript.isContinue)
            {
                isStop = false;
                pausePage.SetActive(false);
                Time.timeScale = 1;
                continueScript.isContinue = false;
            }
            else if(Input.GetKeyDown(KeyCode.Escape) && !isStop )
            {
                isStop = true;
                pausePage.SetActive(true);
                Time.timeScale = 0;
            }
        }
    }

    private void SetScore()
    {
        if (score < 10)
        {
            scoreText.text = "0" + "0" + "0" + score.ToString();
        }
        else if (score >= 10 && score < 100)
        {
            scoreText.text = "0" + "0" + score.ToString();
        }
        else if (score >= 100 && score < 1000)
        {
            scoreText.text = "0" + score.ToString();
        }
        else if (score >= 100)
        {
            scoreText.text = score.ToString();
        }
    }

    void SetHeart()
    {
        if (count == 5)
        {
            count = 0;
            if (playerScript.playerHeart < 3)
            {
                playerScript.playerHeart++;
            }
        }
        if (playerScript.playerHeart == 3)
        {
            image1.overrideSprite = sp1;
            image2.overrideSprite = sp1;
            image3.overrideSprite = sp1;
        }
        if (playerScript.playerHeart == 2)
        {
            image1.overrideSprite = sp1;
            image2.overrideSprite = sp1;
            image3.overrideSprite = sp2;
        }
        if (playerScript.playerHeart == 1)
        {
            image1.overrideSprite = sp1;
            image2.overrideSprite = sp2;
            image3.overrideSprite = sp2;
        }
        if (playerScript.playerHeart == 0)
        {
            image1.overrideSprite = sp2;
            image2.overrideSprite = sp2;
            image3.overrideSprite = sp2;
            isGameover = true;
        }
    }

    void GameOver()
    {
        if (player.transform.position.y <= -30f)
        {
            isGameover = true;
        }
        if (isGameover)
        {
            Time.timeScale = 0;
            overPage.SetActive(true);
            score_Text.text = scoreText.text;
            if (!PlayerPrefs.HasKey("best"))
            {
                PlayerPrefs.SetInt("best", int.Parse(score_Text.text));
            }
            else if (int.Parse(score_Text.text)>PlayerPrefs.GetInt("best"))
            {
                PlayerPrefs.SetInt("best", int.Parse(score_Text.text));
            }
            int best = PlayerPrefs.GetInt("best");
            if (best < 10)
            {
                best_Text.text = "0" + "0" + "0" + best.ToString();
            }
            else if (best >= 10 && best < 100)
            {
                best_Text.text = "0" + "0" + best.ToString();
            }
            else if (best >= 100 && best < 1000)
            {
                best_Text.text = "0" + best.ToString();
            }
            else if (best >= 100)
            {
                best_Text.text = best.ToString();
            }
        }
    }

    void MusicVoice()
    {
        voice = slider.value;
        text.text = slider.value.ToString();
    }
}
