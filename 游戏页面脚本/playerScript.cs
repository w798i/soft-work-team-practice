﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript : MonoBehaviour
{
    public Rigidbody2D rbody;
    public CapsuleCollider2D cc;
    public GameObject bullet;
    public LayerMask Layer;
    public LayerMask airLayer;
    public LayerMask slopeLayer;
    public Animator animator;
    public SpriteRenderer sr;

    public float speed = 5.5f;
    public float jumpSpeed1 = 18;
    public float jumpSpeed2 = 12;
    public Transform tran1;
    public Transform tran2;

    public bool isGround;
    public bool jumpPressed;
    public int jumpCount=2;

    public float shootTime = 1f;
    public static int playerHeart = 3;
    public float attackTime = 1f;
    public float bulletSpeed = 8f;
    public float powerTime = 3f;
    public static float useTime = 0f;
    public static float shakeTime;

    public bool isJump = false;
    public bool isFall = false;
    public static bool isHurt = false;
    public static bool isPower = false;
    public static bool canShoot = false;
    public bool isOnSlope = false;

    public static int count = 0;
    private float horizontalMove;

    // Start is called before the first frame update
    void Start()
    {
        playerHeart = 3;
        isPower = false;
        isHurt = false;
        isJump = false;
        isFall = false;
        canShoot = false;
        count = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("jump") && jumpCount > 0)
        {
            jumpPressed = true;
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            CrossWall();
        }
    }

    void FixedUpdate()
    {
        Onslope();
        Ground();
        Move();
        Jump();
        SwitchAnimator();
        attackTime = attackTime + Time.fixedDeltaTime;
        if (attackTime >= shootTime && Input.GetMouseButton(1)&&canShoot)
        {
            Attack();
            attackTime = 0;
        }
        if (isHurt&&playerHeart!=0)
        {
            Power();
        }
    }

    private void Ground()
    {
        isGround = Physics2D.OverlapCircle(tran1.position, 0.1f, Layer) || Physics2D.OverlapCircle(tran2.position, 0.1f, Layer);
    }

    private void Move()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal");
        rbody.velocity = new Vector2(horizontalMove * speed, rbody.velocity.y);
        if (horizontalMove != 0)
        {
            this.transform.localScale = new Vector3(horizontalMove * 1, 1, 1);
        }
    }

    void Jump()
    {
        if (isGround && !isJump && !isFall)
        {
            jumpCount = 2;
        }
        if (jumpPressed && !isJump && !isFall)
        {
            rbody.velocity = new Vector2(rbody.velocity.x, jumpSpeed1);
            jumpCount--;
        }
        else if (((jumpPressed && !isGround && jumpCount!=0)||(jumpPressed && isGround && jumpCount!=0)) && rbody.velocity.y < jumpSpeed2)
        {
            rbody.velocity = new Vector2(rbody.velocity.x,jumpSpeed2);
            jumpCount--;
        }
        jumpPressed = false;
    }

    void Attack()
    {
        Vector3 m_mousePosition = Input.mousePosition;
        m_mousePosition = Camera.main.ScreenToWorldPoint(m_mousePosition);
        m_mousePosition.z = 0;
        float m_fireAngle = Vector2.Angle(m_mousePosition - this.transform.position, Vector2.up);

        if (m_mousePosition.x > this.transform.position.x)
        {
            m_fireAngle = -m_fireAngle;
        }
        if (this.transform.localScale.x>0&&m_fireAngle <= -30 && m_fireAngle >= -150)
        {
            Vector2 position = new Vector2(transform.position.x + 0.7f, transform.position.y - 0.5f);
            GameObject m_bullet = Instantiate(bullet, position, Quaternion.identity) as GameObject;
            m_bullet.GetComponent<Rigidbody2D>().velocity = ((m_mousePosition - transform.position).normalized * bulletSpeed);
            m_bullet.transform.eulerAngles = new Vector3(0, 0, m_fireAngle);
        }
       else if (this.transform.localScale.x < 0 && m_fireAngle >= 30 && m_fireAngle <= 150)
        {
            Vector2 position = new Vector2(transform.position.x - 0.7f, transform.position.y - 0.5f);
            GameObject m_bullet = Instantiate(bullet, position, Quaternion.identity) as GameObject;
            m_bullet.GetComponent<Rigidbody2D>().velocity = ((m_mousePosition - transform.position).normalized * bulletSpeed);
            m_bullet.transform.eulerAngles = new Vector3(0, 0, m_fireAngle);
        }
    }

    void SwitchAnimator()
    {
        if (isGround )
        {
            if (horizontalMove == 0)
            {
                if (isOnSlope)
                {
                    isJump = false;
                    isFall = false;
                    animator.SetBool("isIdle", true);
                    animator.SetBool("isRun", false);
                    animator.SetBool("isJump", false);
                    animator.SetBool("isFall", false);
                }
                else
                {
                    if (rbody.velocity.y > 0.1f)
                    {
                        isJump = true;
                        isFall = false;
                        animator.SetBool("isIdle", false);
                        animator.SetBool("isRun", false);
                        animator.SetBool("isJump", true);
                        animator.SetBool("isFall", false);
                    }
                    else if (rbody.velocity.y < -0.1f)
                    {
                        isJump = false;
                        isFall = true;
                        animator.SetBool("isIdle", false);
                        animator.SetBool("isRun", false);
                        animator.SetBool("isJump", false);
                        animator.SetBool("isFall", true);
                    }
                    else
                    {
                        isJump = false;
                        isFall = false;
                        animator.SetBool("isIdle", true);
                        animator.SetBool("isRun", false);
                        animator.SetBool("isJump", false);
                        animator.SetBool("isFall", false);
                    }
                }
            }
            else
            {
                if (isOnSlope)
                {
                    isJump = false;
                    isFall = false;
                    animator.SetBool("isIdle", false);
                    animator.SetBool("isRun", true);
                    animator.SetBool("isJump", false);
                    animator.SetBool("isFall", false);
                }
                else if (rbody.velocity.y==0)
                {
                    isJump = false;
                    isFall = false;
                    animator.SetBool("isIdle", false);
                    animator.SetBool("isRun", true);
                    animator.SetBool("isJump", false);
                    animator.SetBool("isFall", false);
                }
            }
            
        }
        else if (!isGround)
        {
            if (rbody.velocity.y > 0)
            {
                isJump = true;
                isFall = false;
                animator.SetBool("isIdle", false);
                animator.SetBool("isRun", false);
                animator.SetBool("isJump", true);
                animator.SetBool("isFall", false);
            }
            else if (rbody.velocity.y < 0)
            {
                isJump = false;
                isFall = true;
                animator.SetBool("isIdle", false);
                animator.SetBool("isRun", false);
                animator.SetBool("isJump", false);
                animator.SetBool("isFall", true);
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            cc.isTrigger = false;
        }
    }

    public static void Hurt(int hurt)
    {
        count++;
        playerHeart = playerHeart - hurt;
        isHurt = true;
        useTime = 0;
        shakeTime = 0;
    }

    private void Power()
    {
        useTime = useTime + Time.fixedDeltaTime;
        shakeTime = shakeTime + Time.fixedDeltaTime;
        if (useTime <= powerTime)
        {
            isPower = true;
            if (shakeTime % 0.2f >= 0.05f)
            {
                sr.enabled = true;
            }
            else
            {
                sr.enabled = false;
            }
        }
        else
        {
            isPower = false;
            isHurt = false;
            count = 0;
        }
    }

    private void CrossWall()
    {
        if (Physics2D.OverlapCircle(tran1.position, 0.00001f, airLayer) || Physics2D.OverlapCircle(tran2.position, 0.00001f, airLayer))
        {
            cc.isTrigger = true;
        }
    }

    private void Onslope()
    {
        if (Physics2D.OverlapCircle(tran1.position, 0.1f, slopeLayer) || Physics2D.OverlapCircle(tran2.position, 0.1f, slopeLayer))
        {
            isOnSlope = true;
        }
        else
        {
            isOnSlope = false;
        }
    }
}
