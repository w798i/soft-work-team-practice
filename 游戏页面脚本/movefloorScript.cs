﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movefloorScript : MonoBehaviour
{
    public string type;
    public float speed;
    public GameObject start;
    public GameObject end;
    public GameObject player;
    public Transform tran1;
    public Transform tran2;
    public int way;
    public bool isPlayerOnFloor = false;

    // Start is called before the first frame update
    void Start()
    {
        if (type.Equals("竖直"))
        {
            way = -1;
        }
        if (type.Equals("水平"))
        {
            way = 1;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (type.Equals("竖直"))
        {
            if (this.transform.position.y <= end.transform.position.y)
            {
                way = 1;
            }
            if (this.transform.position.y >= start.transform.position.y)
            {
                way = -1;
            }
            float y = this.transform.position.y + speed * Time.fixedDeltaTime * way;
            this.transform.position = new Vector3(this.transform.position.x, y, this.transform.position.z);
        }
        else if (type.Equals("水平"))
        {
            if (this.transform.position.x <= start.transform.position.x)
            {
                way = 1;
            }
            if (this.transform.position.x >= end.transform.position.x)
            {
                way = -1;
            }
            float x = this.transform.position.x + speed * Time.fixedDeltaTime * way;
            this.transform.position = new Vector3(x, this.transform.position.y, this.transform.position.z);
        }
        if (isPlayerOnFloor&& type.Equals("竖直")&&way!=1)
        {
            float y= player.transform.position.y + speed * Time.fixedDeltaTime * way;
            player.transform.position = new Vector3(player.transform.position.x, y, this.transform.position.z);
        }
        else if(isPlayerOnFloor && type.Equals("水平"))
        {
            float x = player.transform.position.x + speed * Time.fixedDeltaTime * way;
            player.transform.position = new Vector3(x, player.transform.position.y, this.transform.position.z);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isPlayerOnFloor = true;
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isPlayerOnFloor = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isPlayerOnFloor = false;
        }
    }
}
