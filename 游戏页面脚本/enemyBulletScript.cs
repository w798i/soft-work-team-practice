﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBulletScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Floor": Destroy(gameObject); break;
            case "Player": if (!playerScript.isPower && playerScript.count == 0) { playerScript.Hurt(1); } Destroy(gameObject); break;
            case "MoveFloor": Destroy(gameObject); break;
            case "Air": Destroy(gameObject); break;
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
