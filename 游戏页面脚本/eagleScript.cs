﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eagleScript : MonoBehaviour
{
    public int heart = 5;
    public int hurt = 1;
    public GameObject die;
    public GameObject player;
    public GameObject bullet;
    public float bulletSpeed = 12f;
    public float shootTime = 2;
    public float attackTime = 1.5f;
    public int score = 15;
    public bool isVisible = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (player.transform.position.x <= this.transform.position.x)
        {
            this.transform.localScale = new Vector3(10f, this.transform.localScale.y, this.transform.localScale.z);
        }
        else
        {
            this.transform.localScale = new Vector3(-10f, this.transform.localScale.y, this.transform.localScale.z);
        }
    }

    private void FixedUpdate()
    {
        attackTime = attackTime + Time.fixedDeltaTime;
        if (isVisible)
        {
            if (attackTime >= shootTime)
            {
                Attack();
                attackTime = 0;
            }
        }
        if (heart == 0)
        {
            Die();
        }
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }

    private void OnBecameInvisible()
    {
        isVisible = false;
    }
    private void Die()
    {
        Instantiate(die, transform.position, transform.rotation);
        Destroy(gameObject);
        GameManager.score = GameManager.score + score;
    }

    void Attack()
    {
        if (this.transform.localScale.x > 0)
        {
            Vector2 position = new Vector2(transform.position.x - 1.5f, transform.position.y - 0.8f);
            Vector3 playerPosition = player.transform.position;
            float m_fireAngle = Vector2.Angle(playerPosition - this.transform.position, Vector2.up);
            GameObject m_bullet = Instantiate(bullet, position, Quaternion.identity) as GameObject;
            m_bullet.GetComponent<Rigidbody2D>().velocity = ((playerPosition - transform.position).normalized * bulletSpeed);
            m_bullet.transform.eulerAngles = new Vector3(0, 0, m_fireAngle);
        }
        else if (this.transform.localScale.x < 0)
        {
            Vector2 position = new Vector2(transform.position.x +1.5f, transform.position.y - 0.8f);
            Vector3 playerPosition = player.transform.position;
            float m_fireAngle = Vector2.Angle(playerPosition - this.transform.position, Vector2.up);
            GameObject m_bullet = Instantiate(bullet, position, Quaternion.identity) as GameObject;
            m_bullet.GetComponent<Rigidbody2D>().velocity = ((playerPosition - transform.position).normalized * bulletSpeed);
            m_bullet.transform.eulerAngles = new Vector3(0, 0, m_fireAngle);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Player": if (!playerScript.isPower && playerScript.count == 0) { playerScript.Hurt(hurt); } break;
            case "PlayerBullet": heart = heart - 1; break;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Player": if (!playerScript.isPower && playerScript.count == 0) { playerScript.Hurt(hurt); } break;
        }
    }
}
