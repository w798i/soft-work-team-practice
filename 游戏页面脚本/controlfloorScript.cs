﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlfloorScript : MonoBehaviour
{
    public GameObject player;
    public GameObject start;
    public GameObject end;
    public GameObject sign;
    public float speed;
    public int way;
    public bool isOnFloor = false;
    public static bool isRun = false;
    // Start is called before the first frame update
    void Start()
    {
        way = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (stickScript.isOpen)
        {
            isRun = true;
        }
        if (isRun)
        {
            if (this.transform.position.y < end.transform.position.y)
            {
                way = 1;
                isRun = false;
            }
            if (this.transform.position.y > start.transform.position.y)
            {
                way = -1;
                isRun = false;
            }
            float y = this.transform.position.y + speed * Time.fixedDeltaTime * way;
            this.transform.position = new Vector3(this.transform.position.x, y, this.transform.position.z);
        }
        if (isOnFloor&&isRun)
        {
            float player_y = player.transform.position.y + speed * Time.fixedDeltaTime * way;
            float sign_y = sign.transform.position.y + speed * Time.fixedDeltaTime * way;
            player.transform.position = new Vector3(player.transform.position.x, player_y, this.transform.position.z);
            sign.transform.position = new Vector3(sign.transform.position.x, sign_y, this.transform.position.z);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isOnFloor = true;
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isOnFloor = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            isOnFloor = false;
        }
    }

}
