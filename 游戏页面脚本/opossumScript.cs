﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class opossumScript : MonoBehaviour
{
    public int heart = 5;
    public int hurt = 1;
    public float speed = 5;
    public GameObject right;
    public GameObject left;
    public GameObject die;
    public int score = 10;
    public int way;
    private float lsx;

    // Start is called before the first frame update
    void Start()
    {
        lsx = -this.transform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        Run();
        if (heart == 0)
        {
            Die();
        }
    }

    void Run()
    {
        if (this.transform.position.x >= right.transform.position.x)
        {
            way = -1;
        }
        if (this.transform.position.x <= left.transform.position.x)
        {
            way = 1;
        }
        float x = this.transform.position.x + speed * Time.fixedDeltaTime * way;
        this.transform.position = new Vector3(x, this.transform.position.y, this.transform.position.z);
        this.transform.localScale = new Vector3(way*lsx, this.transform.localScale.y, this.transform.localScale.z);
    }

    private void Die()
    {
        Instantiate(die, transform.position, transform.rotation);
        Destroy(gameObject);
        GameManager.score = GameManager.score + score;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Player": if (!playerScript.isPower&&playerScript.count==0) { playerScript.Hurt(hurt);}break;
            case "PlayerBullet": heart = heart - 1; break;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Player": if (!playerScript.isPower && playerScript.count == 0) { playerScript.Hurt(hurt); } break;
        }
    }
}
