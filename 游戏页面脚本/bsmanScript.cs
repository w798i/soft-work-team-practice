﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bsmanScript : MonoBehaviour
{
    public Transform playerTransform;
    public GameObject page;
    public GameObject signText;
    public GameObject gm;
    public Text text;
    public bool isOpen=false;
    public static bool isSign = false;
    public static bool isFirst = true;
    string str1= "嘿,你好.可以帮我一个忙吗?我的钻石被一只怪物抢走了,你能帮我把它夺回来吗?对了,这个给你.当你按下鼠标右键时,它会沿着你的鼠标方向发射出一颗子弹,用它去打败怪物吧,拜托你了.";
    string str2="加油!拜托你了!";

    void Start()
    {
        isFirst = true;
    }

    void Update()
    {
        if (this.transform.position.x-playerTransform.position.x<=0.7f && this.transform.position.x - playerTransform.position.x >= -0.8f && this.transform.position.y - playerTransform.position.y <= 0.7f && this.transform.position.y - playerTransform.position.y >= -0.8f)
        {
            signText.SetActive(true);
            isSign = true;
        }
        else
        {
            signText.SetActive(false);
            isSign = false;
        }
        if (isSign && Input.GetKeyDown(KeyCode.E)&&!isOpen && !GameManager.isStop)
        {
            page.SetActive(true);
            isOpen = true;
            Time.timeScale = 0;
        }
        else if (Input.GetKeyDown(KeyCode.E) && isOpen)
        {
            page.SetActive(false);
            isOpen = false;
            Time.timeScale = 1;
        }
        if (isFirst && page.activeInHierarchy)
        {
            text.text = str1;
            isFirst = false;
            playerScript.canShoot = true;
        }
        if(!isFirst&&!page.activeInHierarchy)
        {
            text.text = str2;
        }
    }
}
